const request = require("request");
const numericRequirementParser = requirement => {
  let match = requirement.match(/^([+-]?)(\d+)([+-]?)$/);
  if (requirement === "none") return val => val === undefined;
  if (match && match[1])
    return val =>
      (parseInt(val) - parseInt(match[2])) * (match[1] === "+" ? 1 : -1) > 0;
  if (match && match[3])
    return val =>
      (parseInt(val) - parseInt(match[2])) * (match[3] === "+" ? 1 : -1) > -1;
  match = requirement.match(/^\d+$/);
  if (match) return val => parseInt(val) === parseInt(requirement);
};
const ranks = {
  mineplex: [undefined, "ultra", "hero", "legend", "titan", "eternal"],
  hypixel: [undefined, "vip", "vip_plus", "mvp", "mvp_plus", "mvp_plus_plus"]
};
const rankRequirementParser = (requirement, server) => {
  if (requirement === "none") return val => val === undefined;
  let rank = requirement.replace(/[\+-]/g, "");
  let rankIndex = ranks[server].indexOf(rank);
  if (rankIndex === -1)
    throw new TypeError(`Invalid rank value ${rank} on server ${server}`);
  let parser = numericRequirementParser(
    requirement.replace(new RegExp(rank, "gi"), rankIndex)
  );
  return val => parser(ranks[server].indexOf(val));
};
const requirementExpression = str => {
  str = str.replace(/\s+/g, "");
  let firstParenthesis = null;
  let nestedLevel = 0;
  let subExpressions = [];
  const requirementParsers = {
    HYPIXEL_LEVEL: (obj, requirement) =>
      numericRequirementParser(requirement)(obj["hypixel.lvl"]),
    HYPIXEL_RANK: (obj, requirement) =>
      rankRequirementParser(requirement, "hypixel")(obj["hypixel.rank"]),
    MINEPLEX_LEVEL: (obj, requirement) =>
      numericRequirementParser(requirement)(obj["mineplex.lvl"]),
    MINEPLEX_RANK: (obj, requirement) =>
      rankRequirementParser(requirement, "mineplex")(obj["mineplex.rank"]),
    SUB_EXPRESSION: (obj, i) => subExpressions[i](obj)
  };
  for (let i = 0; i < str.length; i++) {
    if (str[i] === "(") {
      if (firstParenthesis === null) firstParenthesis = i;
      nestedLevel++;
    }
    if (str[i] === ")") {
      nestedLevel--;
      if (!nestedLevel) {
        let strArr = [...str];
        strArr.splice(
          firstParenthesis,
          i - firstParenthesis + 1,
          `SUB_EXPRESSION[${subExpressions.push(
            requirementExpression(str.slice(firstParenthesis + 1, i))
          ) - 1}]`
        );
        str = strArr.join("");
        firstParenthesis = null;
      }
    }
  }
  return obj => {
    let str2 = str;
    str2 = str2.replace(/([A-Z_]+)\[([a-z_\d+\-]+)\]/g, (match, p1, p2) =>
      requirementParsers[p1](obj, p2)
        .toString()
        .toUpperCase()
    );
    let match;
    let i = 0;
    while (!["TRUE", "FALSE"].includes(str2)) {
      i++;
      if (i >= 15) throw new Error("Expression too complex or invalid!");
      str2 = str2.replace(/(TRUE|FALSE)\|(TRUE|FALSE)/g, (match, p1, p2) => {
        return [p1, p2]
          .includes("TRUE")
          .toString()
          .toUpperCase();
      });
      str2 = str2.replace(/(TRUE|FALSE)&(TRUE|FALSE)/g, (match, p1, p2) => {
        return (p1 === "TRUE" && p2 === "TRUE").toString().toUpperCase();
      });
      str2 = str2.trim();
    }
    return str2 === "TRUE";
  };
};
module.exports = config => {
  return new Promise((resolve, reject) => {
    const requirement = requirementExpression(config.requirements);
    const key = config.key;
    let alts = [];
    let findAlt = () => {
      request(
        {
          url: `https://api.thealtening.com/v1/generate?info=true&token=${encodeURIComponent(key)}`,
          rejectUnauthorized: false
        },
        (err, res, body) => {
          if (err) throw new Error(err);
          const bodyObj = JSON.parse(body);
          if (bodyObj.limit && !bodyObj.token) {
            alts.push({ limit: true });
            return resolve(alts);
          }
          try {
            if (requirement(bodyObj.info)) {
              alts.push({
                failed: false,
                token: bodyObj.token,
                username: bodyObj.username
              });
              return resolve(alts);
            }
          } catch (e) {
            return resolve("An error occured");
          }
          alts.push({
            failed: true,
            token: bodyObj.token,
            username: bodyObj.username
          });
          findAlt();
        }
      );
    };
    findAlt();
  });
};

