require("dotenv").config();
const Discord = require("discord.js");
const request = require("request");
const finder = require("./finder");
const sortBy = require("lodash.sortby");
const ranks = {
  mineplex: [undefined, "ultra", "hero", "legend", "titan", "eternal"],
  hypixel: [undefined, "vip", "vip_plus", "mvp", "mvp_plus", "mvp_plus_plus"]
};
const client = new Discord.Client();
client.on("message", message => {
  const isSort = message.content.startsWith("|api");
  if (message.channel.type !== "dm" || (!message.content.startsWith("api-") && !isSort))
    return;
  const key = isSort ? message.content.slice(1) : message.content;
  request(
    {
      url:
        "https://api.thealtening.com/v1/license?token=" +
        encodeURIComponent(key),
      rejectUnauthorized: false
    },
    async (err, res, body) => {
      if (err) {
        console.log(err);
        return message.channel.send("An error occured.");
      }
      const bodyJSON = JSON.parse(body);
      let autoPrivate = false;
      if (isSort) {
        message.channel.send(`Welcome, **${bodyJSON.username}**. Would you rather sort by Mineplex or Hypixel rank?`);
        const messages = await message.channel.awaitMessages(
          m =>
            (m.content.toLowerCase().startsWith("m") ||
              m.content.toLowerCase().startsWith("h")) &&
            m.author.id === message.author.id,
          { maxMatches: 1, time: 30000 }
        );
        if (!messages.size)
          return message.channel.send("You didn't answer in time!");
        const hypixel = messages.first().content.toLowerCase().startsWith("h");
        message.channel.send("How many alts do you want to generate?");
        const messages2 = await message.channel.awaitMessages(
          m => (~~m.content &&
            m.author.id === message.author.id),
          { maxMatches: 1, time: 30000 }
        );
        if (!messages2.size)
          return message.channel.send("You didn't answer in time!");
        const max = ~~messages2.first().content;
        let i = 0;
        let alts = [];
        const getAlt = resolve => {
          request(
        {
          url: `https://api.thealtening.com/v1/generate?info=true&token=${encodeURIComponent(key)}`,
          rejectUnauthorized: false
        }, (err, res, body) => {
          if (err) return resolve(), message.channel.send("An error occured during the generation of an alt!");
          const bodyObj = JSON.parse(body);
          alts.push(bodyObj);
          i++;
          if (i < max && !bodyObj.limit) setTimeout(() => getAlt(resolve), 500); else resolve();
        });
        };
        await new Promise((resolve, reject) => getAlt(resolve));
        request.post({
      url: "https://pastr.io/api/create",
      headers: {
          "Content-Type": "application/json"
      },
      body: JSON.stringify({paste: sortBy(alts, [
        a => -(hypixel ? ranks.hypixel : ranks.mineplex).indexOf(a.info[`${hypixel ? "hypixel" : "mineplex"}.rank`]),
        a => -~~a.info[`${hypixel ? "hypixel" : "mineplex"}.lvl`]
      ]).map(a => `${a.username}, ${a.token}`).join("\n"), syntax: "js", destruct: "destructonread"})
  }, (err, res, body) => {
      if (err) return message.channel.send("An error occurred during the alt list uploading!");
      message.channel.send(`**<https://pastr.io/${JSON.parse(body).data.url}>** is your alt list!`);
  });
      } else {
      if (bodyJSON.premium_name === "premium") {
        message.channel.send(
          `Welcome, **${bodyJSON.username}**. Would you like to auto-private alts that I find (currently a broken endpoint)? (Y/N)`
        );
        const messages = await message.channel.awaitMessages(
          m =>
            (m.content.toLowerCase().startsWith("y") ||
              m.content.toLowerCase().startsWith("n")) &&
            m.author.id === message.author.id,
          { maxMatches: 1, time: 30000 }
        );
        if (!messages.size)
          return message.channel.send("You didn't answer in time!");
        autoPrivate = false; //messages.first().content.toLowerCase().startsWith("y");
        message.channel.send(
          "What requirements do you want the alts to satisfy?"
        );
      } else {
        message.channel.send(
          `Welcome, **${bodyJSON.username}**. What requirements do you want the alts to satisfy?`
        );
      }
      const collector = message.channel.createMessageCollector(
        m => m.author.id === message.author.id,
        { maxMatches: 1, time: 30000 }
      );
      collector.on("collect", async m => {
        collector.stop();
        const requirements = m.content;
        const alts = await finder({ requirements, key });
        if (typeof alts === "string")
          return message.channel.send("An error occured.");
        const limitReached = alts[alts.length - 1].limit;
        const messageData = {
          embed: new Discord.RichEmbed()
            .setTitle("Result")
            .setDescription(limitReached ? "Limit reached" : "Found alt")
            .addField("Alts generated", alts.filter(a => !a.limit).length)
            .setColor(Math.floor(Math.random() * 0x1000000))
            .setFooter("TheAltening Finder")
            .setTimestamp()
        };
        if (!limitReached) {
          messageData.embed
            .addField("Username", alts[alts.length - 1].username, true)
            .addField("Token", alts[alts.length - 1].token, true);
          if (autoPrivate) {
            request(
              {
                url:
                  "https://api.thealtening.com/v1/private?token=" +
                  encodeURIComponent(key) +
                  "&acctoken=" +
                  encodeURIComponent(alts[alts.length - 1].token),
                rejectUnauthorized: false
              },
              (err, res, body) => {}
            );
            messageData.embed.setFooter("Alt has been privated");
          }
        }
        message.channel.send(messageData);
      });
      collector.on("end", collected => {
        if (!collected.size)
          return message.channel.send("You didn't answer in time!");
      });
    }
    }
  );
});
client.on("ready", () => {
  console.log("Client has logged in!");
});
client.login(process.env.DISCORD_BOT_TOKEN);
